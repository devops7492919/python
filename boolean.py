# Boolean Values
# In programming you often need to know if an expression is True or False.
# You can evaluate any expression in Python, and get one of two answers, True or False.
# When you compare two values, the expression is evaluated and Python returns the Boolean answer.

a = 15
b = 10
x = "The value of a and b is ", a>b
y = "The value of a and b is ", a<b
z = "The value of a and b is ", a==b
v = "The value of a and b is ", a!=b
print((x), (y), (z), (v) )


# Python program
 
a = 91
b = 73

if a > b :
    print( "a is greater than b ")

else:
    print("a is less than b ")



# Evaluate Values and Variables
# The bool() function allows you to evaluate any value, and give you True or False in return,

print(bool("hello"))
print(bool(15))