a = "harry"
b = 345
c = 45.45
d = True
e = False


# agar kuch nahi hai dikhana hai toh none type karna 
f = None

# Printing the variables 
print(a)
print(b)
print(c)
print(d)
print(e)
print(f)

#printing the type of variables
print(type(a))
print(type(b))
print(type(c))
print(type(d))
print(type(e))
print(type(f))

