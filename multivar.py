x, y, z = "orange", "banana", "grapes"
print(x,y,z)

"""One Value to Multiple Variables
And you can assign the same value to multiple variables in one line:"""

x = y = z = "shivansh"
print(x,y,z)

"""Unpack a Collection
If you have a collection of values in a list, tuple etc. 
Python allows you to extract the values into variables. This is called unpacking."""
fruits = ["deepu", "arati", "shivansh"]
a,b,c = fruits
print(a)
print(b)
print(c)



fruits = ["apple", "banana", "cherry"]
x, y, z = fruits
print(x)
print(y)
print(z)

# https://www.codewithharry.com/notes/