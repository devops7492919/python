#strings is a sequence of characters enclosed in qoutes
# single          a = 'deepak'

# multiple        b = '''deepu'''

# double          c = "deepu"

#Python - Slicing Strings

a = "deepak sabane"
print(a[0:12])

# negative strings 

b = "Hello, World!"
print(b[-3:-12])

b = "Hello, World!"
print(b[-5:-2])

d = "deepaksabane"
print(d[-9:-4])

#Python - Modify Strings

a = "hello, World"
print(a.upper())

b = "DeepakSABANE"
print(b.lower())

c= '''deepak sabane  , shivansh sabane !! '''
print(c.upper())


# Remove Whitespace
# Whitespace is the space before and/or after the actual text, and very often you want to remove this space.

a = "hello , world !! this time we will rock "
print(a.strip())

d = " Hello, World! "
print(d.strip()) # returns "Hello, World!"


# Replace String
# The replace() method replaces a string with another string:


a = "sabane deepu"
print(a.replace("sabane", "dixit"))


b = "b Arati"
print(b.replace("b", "sabane"))

# Split String
# The split() method returns a list where the text between the specified separator becomes the list items.

a = "engineering!! college bidar"
print(a.split())

# String Concatenation
# To concatenate, or combine, two strings you can use the + operator.

a = "shivansh"
b = "sabane"
print(a+b)


# Example
# To add a space between them, add a " ":


a = "shivansh"
b = "sabane"
c = a + " " + b
print(c)

d = "guru nanak dev"
e = "engineering college bidar"
c = d + " " + e
print(c)

# String Format
# As we learned in the Python Variables chapter, we cannot combine strings and numbers like this:

# a = 36
# b = "my name is deepak age is", + age
# print(a+b)

a = 6360696947
b = "my phone number is , {}"
print(b.format(a))


g = " my name is  Deepak, my age is {}"
age = "28"
print(g.format(age))


quantity = 3
itemno = 567
price = 49.95
myorder = "I want {} pieces of item {} for {} dollars."
print(myorder.format(quantity, itemno, price))



# The format() method takes unlimited number of arguments, and are placed into the respective placeholders:
quantity = 10 
itemnumber = 438
price = 101
myoreder = "I want {} quantity of icecream of itemnumber {}  for the rate of {}. "
print(myorder.format(quantity,itemnumber,price))


# Escape Character
# To insert characters that are illegal in a string, use an escape character.
# An escape character is a backslash \ followed by the character you want to insert.
# An example of an illegal character is a double quote inside a string that is surrounded by double quotes:

# a = "iam the only one thinking to become "millionere" by doing hardwork and  got it "
a = "iam the only one thinking to become \"millionere\" by doing hardwork and  smartwork  "

print(a)

#txt = "We are the so-called "Vikings" from the north."
txt = "We are the so-called \"Vikings\" from the north."
print(txt)

#txt2 = "har har "mahadev" devon k dev mahadev"
txt2 = "har har \"mahadev\" devon k dev mahadev"
print(txt2)

# \'	Single Quote	
# \\	Backslash	
# \n	New Line	
# \r	Carriage Return	
# \t	Tab	
# \b	Backspace	
# \f	Form Feed	
# \ooo	Octal value	
# \xhh	Hex value

#for single qoute 
txt = 'It\'s hot '
print(txt) 

txt2 = 'that\'s summer'
print(txt2)
 
txt3 = 'what\'s the issue'
print(txt3)

txt4 = 'my friend\'s are billioneres'
print(txt4)

# \\	Backslash	
txt = "This will insert one \\ (backslash)."
print(txt) 

txt2 = "iam not aware whats happening \\ "
print(txt2)

# \n	New Line

txt = "Hello\nWorld!"
print(txt)

txt2 = "my name is \nhelloworld!!"
print(txt2)

a = "everything is temporary in life  \n Enjoy each and every moment"
print(a)

#\r	Carriage Return

txt = "Hello\rWorld!"
print(txt) 


b = "Aashiqbanaya\rApne"
print(b)

# \ooo	Octal value	
# \xhh	Hex value

#A backslash followed by an 'x' and a hex number represents a hex value:
txt = "\x48\x65\x6c\x6c\x6f"
print(txt) 

#A backslash followed by three integers will result in a octal value:
txt1 = "\110\145\154\154\157"
print(txt1) 

# Python String count() Method

txt = "I love apples, apple are my favorite fruit "

x = txt.count("apple")

print(x)

txt = "hello, hello, hello"
x = txt.count("hello")
print(x)

a = "deepak, deepak, deepak, deepak"
x = a.count("deepak")
print(x)

b = "my favourite numbers is 400 , 400"
x = b.count("400")
print(x)

d = '''This is my favourite string "abaabaaba" '''
h = d.count("aba")
print(h)

#Python String encode() Method

a = "my name is deepak"
s = a.encode()
print(s)

print(txt.encode(encoding="ascii",errors="backslashreplace"))


txt = "my name is shivansh"
x = txt.encode()
print(x)


txt1 = "Hello, world!"
x = txt1.encode()
print(x)

# The endswith() method in Python is used to check whether a string ends with a specified suffix. It returns True if the string ends with the specified suffix, and False otherwise.

# The syntax for using the endswith() method is as follows:

# python
# Copy code
# string.endswith(suffix[, start[, end]])

txt = "Hello, welcome to my world."

x = txt.endswith(".")

print(x)

txt2 = "my name is deepak.got it!!"
x = txt2.endswith("!!")

txt = "Hello, welcome to my world."
result = txt.endswith("world.")
print(result)

txt3 = "hello, welcome to jumanji bidar."
result = txt3.endswith("deepak bidar.")
print(result)

#Set the tab size to 2 whitespaces:
txt = "H\te\tl\tl\to"

x =  txt.expandtabs(2)

print(x)


a = "D\te\te\tp\ta\tk"
x = a.expandtabs(7)
print(x)

b = "A\tR\tA\tT\tI"
d =  b.expandtabs(7)
print(d)


#Python String find() Method

zfile = "welcome to the jumanji world"
v = zfile.find("world")
print(v)


txt = "Hello, welcome to my world."

x = txt.find("e")

print(x)


txt = "Hello, welcome to my world."

x = txt.find("e", 5, 10)

print(x)

# Python String partition() Method

g = "myself deepak I enjoyed alot "
h = g.partition("deepak")
print(h)

k = "i wanted to become a good devops engineer who knows python "
l = k.partition("devops")
print(l)


# Python String join() Method
mytuple = "deepak", "shivansh"
x = "#".join(mytuple)
print(x)

pixelvide = "deepak", "DevOps"
x = "#".join(pixelvide)
print(x)

